/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	'new': function(req, res) {
	    if (!req.session.authenticated) {
	        res.view();
	    }
	    else {
	        res.forbidden();
	    }
	},
	'create': function(req, res, next) {
		User.create(req.params.all(), function userCreated(err, user) {
		    if(err) {
		        return res.serverError(err, './user/new');
		    }
			if(req.wantsJSON){
			    return res.json(user);
			}
			else {
			    return res.login({
			        username: user.username,
			        password: req.param('password'),
			        successRedirect: '/',
			        invalidRedirect: '/'
			    });
			}
		});
	},
	login: function (req, res) {
    // See `api/responses/login.js`
        return res.login({
            username: req.param('username'),
            password: req.param('password'),
            successRedirect: '/',
            invalidRedirect: '/'
        });
    },
    logout: function (req, res) {

        // "Forget" the user from the session.
        // Subsequent requests from this user agent will NOT have `req.session.me`.
        req.session.authenticated = null;
        req.session.isAdmin = null;
    
        // If this is not an HTML-wanting browser, e.g. AJAX/sockets/cURL/etc.,
        // send a simple response letting the user agent know they were logged out
        // successfully.
        if (req.wantsJSON) {
          return res.ok('Logged out successfully!');
        }
    
        // Otherwise if this is an HTML-wanting browser, do a redirect.
        return res.redirect('/');
    },
    index: function( req, res, next) {
        User.find(function foundUsers (err, users) {
            if (err) return next(err);
            if (req.wantsJSON){
                res.json(users);
            }
            else {
                res.view({
                    users: users
                });
            }
        });
    },
    show: function(req, res, next) {
        User.findOne(req.param('id'), function(err, user) {
            if (err) return next(err);
            if (!user) return next();
            res.view({
                user: user
            });
        });
    },
    edit: function(req, res, next) {
        User.findOne(req.param('id'), function(err, user) {
            if (err) return  next(err);
            if (!user) return res.notFound;
            else {
                res.view({
                    user: user
                });
            }
        });
    }
};