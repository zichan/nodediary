/**
 * EntryController
 *
 * @description :: Server-side logic for managing Entries
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  new: function(req, res, next) {
      res.view();
  },
  index: function(req, res, next) {
    var id = req.param('id');
    var idShortCut = isShortcut(id);
    if (idShortCut === true) {
      return next();
    }
    if (id) {
      Entry.findOne(id, function(err, Entry) {
        sails.log.debug(id);
        if (Entry === undefined) return res.notFound();
        if (err) return next(err);
        if (Entry.author != req.session.user.id) {
          console.log("User unauthorized");
          return res.json({
            Title: "Unauthorized",
            Body: "You are not permitted to view this Entry"
          });
        }
        else {
          moment = require('moment');
          Entry.rangeStart = moment(Entry.rangeStart).format('YYYY-MM-DD');
          Entry.rangeEnd = moment(Entry.rangeEnd).format('YYYY-MM-DD');
          sails.log.debug('User permitted');
          return res.json(Entry);
        }
      });
    } else {
      var where = req.param('where');
      if (_.isString(where)) {
        where = JSON.parse(where);
      }
      var options = {
        limit: req.param('limit') || undefined,
        skip: req.param('skip') || undefined,
        sort: req.param('sort') || undefined,
        author: req.session.user.id
      };
      Entry.find(options, function(err, entry) {
        if (entry === undefined) return res.notFound();
        if (err) return next(err);
          res.json(entry);
      });
    }
    function isShortcut(id) {
      if (id === 'find' || id === 'update' || id === 'create' || id === 'destroy') {
        return true;
      }
    }
  },
  create: function(req, res, next) {
    Entry.create(req.params.all()).exec(function (err, entry) {
      if(err) return next(err);
      if (entry) {
        User.find({id:req.session.user.id}).populate('entrys').exec(function(err, user) {
          if (err) res.serverError("Something", 500);
          else {
            user[0].entrys.add(entry.id);
            user[0].save(function(err) {
              if (err) return res.serverError();
              else {
                if(req.wantsJSON) {
                  return res.ok;
                }
                else {
                  res.redirect('/');
                }
              }
            });
          }
        });
      }
    });
  },
  show: function(req, res, next) {
    res.view({
      entry: req.param('id')
    });
  },
  edit: function(req, res, next) {
    Entry.findOne({id: req.param('id')}, function (err, entry) {
      if (err) sails.log.debug("Error at edit in EntryController.js \n " + err);
      else {
        res.view({
          entry: entry,
          moment: require('moment')
        });
      }
    });
  }
};