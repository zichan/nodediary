/**
* Entry.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    'Title': {
      type: 'string',
      required: true,
    },
    'Body': {
      type: 'string',
      required: true,
    },
    'rangeStart': {
      type: 'date',
      required: true,
    },
    'rangeEnd': {
      type: 'date',
      required: true,
    },
    'author': {
      model: 'user',
    },
  },
};