/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema: true,
  attributes: {
    firstName: {
      type: 'string',
      required: true,
      unique: false,
    },
    lastName: {
      type: 'string',
      required: true,
      unique: false,
    },
    username: {
      type: 'string',
      required: true,
      unique: true,
    },
    password: {
      type: 'string',
      required: true
    },
    isAdmin: {
      type: 'boolean',
      defaultsTo: false
    },
    entrys: {
      collection: 'entry',
      via: 'author',
    },
    toJSON: function() {
      var obj = this.toObject();
      delete obj.password;
      return obj;
    }
  },
  beforeCreate: function(user, cb) {
    var bcrypt = require('bcrypt');
    bcrypt.genSalt(10, function(err, salt) {
      bcrypt.hash(user.password, salt, function(err, hash) {
        if (err) {
          console.log(err);
          cb(err);
        }else{
          user.password = hash;
          cb(null, user);
        }
      });
    });
  },
  attemptLogin: function (inputs, cb) {
    // Create a user
    var bcrypt = require('bcrypt');
    bcrypt.genSalt(10, function(err, salt) {
      bcrypt.hash(inputs.password, salt, function(err, hash) {
        if (err) {
          console.log(err);
          cb(err);
        }else{
          User.findOne({
            username: inputs.username
          })
          .exec(cb);
        }
      });
    });
  },
  beforeUpdate: function(user, cb) {
    if (user.isAdmin === undefined) {
      user.isAdmin = false;
    }
    cb(null, user);
  }
};